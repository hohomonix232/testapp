﻿using ApplicationForIdeom.Domain;
using ApplicationForIdeom.Domain.Models.FilterModels;
using ApplicationForIdeom.Domain.Models.FilterModels.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationForIdeom.Business.Interfaces
{
    public interface IPhonebookService
    {
        public void Create(Phonebook phonebook);

        public void Update(Phonebook phonebook);

        public Task<List<Phonebook>> GetAll();

        public Task<Phonebook> GetById(int Id);

        public void Delete(int Id);

        public IEnumerable<Phonebook> GetPagedData(IEnumerable<Phonebook> phonebooks, int pageNumber, int pageSize);

        public IEnumerable<Phonebook> Sorting(List<Phonebook> phonebooks, string sort);

        public IEnumerable<Phonebook> Filtering(List<Phonebook> phonebooks, PhonebookFilterModel filter);

        public IEnumerable<Phonebook> AddSelectOptions(PhonebookFilterModel? filter, List<Phonebook> phonebooks, PaginationFilter validFilter);
    }
}
