﻿using ApplicationForIdeom.Business.Interfaces;
using ApplicationForIdeom.Domain;
using ApplicationForIdeom.Domain.Models.FilterModels;
using ApplicationForIdeom.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationForIdeom.Business.Services
{
    public class PhonebookService : IPhonebookService
    {
        private readonly IPhonebookRepository _phonebookRepository;

        public PhonebookService(IPhonebookRepository phonebookRepository)
        {
            _phonebookRepository = phonebookRepository;
        }

        public IEnumerable<Phonebook> AddSelectOptions(PhonebookFilterModel? filter, List<Phonebook> phonebooks, PaginationFilter validFilter)
        {
            Sorting(phonebooks, filter.Sort);

            var filteredPhonebook = Filtering(phonebooks, filter);
            var pagedPhonebooks = GetPagedData(filteredPhonebook, validFilter.PageNumber, validFilter.PageSize);

            return pagedPhonebooks;
        } 

        public void Create(Phonebook phonebook)
        {
            _phonebookRepository.Create(phonebook);
        }

        public void Delete(int Id)
        {
            _phonebookRepository.Delete(Id);
        }

        public void Update(Phonebook phonebook)
        {
            _phonebookRepository.Update(phonebook);
        }

        public async Task<Phonebook> GetById(int Id)
        {
            return await _phonebookRepository.GetById(Id);
        }

        public async Task<List<Phonebook>> GetAll()
        {
            return await _phonebookRepository.GetAll();
        }

        public IEnumerable<Phonebook> GetPagedData(IEnumerable<Phonebook> phonebooks, int pageNumber, int pageSize)
        {
            return _phonebookRepository.GetPagedData(phonebooks, pageNumber, pageSize);
        }

        public IEnumerable<Phonebook> Sorting(List<Phonebook> phonebooks, string sort)
        {
            if (!string.IsNullOrEmpty(sort))
            {
                return _phonebookRepository.Sorting(phonebooks, sort);
            }
            return phonebooks;
        }

        public IEnumerable<Phonebook> Filtering(List<Phonebook> phonebooks, PhonebookFilterModel filter)
        {
            return _phonebookRepository.Filtering(phonebooks, filter);
        }

    }
}
