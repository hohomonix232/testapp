﻿using ApplicationForIdeom.Business.DTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationForIdeom.Business.Models.Validators
{
    public class PhonebookDtoValidator : AbstractValidator<PhonebookDTO>
    {
        public PhonebookDtoValidator()
        {
            var zipCodeRegex = "^(([0-9]{5})*-([0-9]{4}))|([0-9]{5})$";
            var phoneNumberRegex = @"^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$";

            RuleFor(f => f.ZipCode).NotEmpty().Matches(zipCodeRegex).WithMessage("Please specify a valid zipcode.");
            RuleFor(f => f.PhoneNumber).NotEmpty().Matches(phoneNumberRegex).WithMessage("Please specify a valid Russian phonenumber.");
            RuleFor(f => f.Email)
                .NotEmpty().WithMessage("Email address is required.")
                .EmailAddress().WithMessage("Please specify a valid email address.");
        }
    }
}
