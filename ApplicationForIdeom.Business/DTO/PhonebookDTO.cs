﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationForIdeom.Business.DTO
{
    public class PhonebookDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}
