﻿using ApplicationForIdeom.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ApplicationForIdeom.Business.Interfaces
{
    public interface IPhonebookService 
    {
        Phonebook GetById(int id);
        IEnumerable<Phonebook> GetAll();
        IEnumerable<Phonebook> Find(Expression<Func<Phonebook, bool>> expression);
        void Add(Phonebook entity);
        void AddRange(IEnumerable<Phonebook> entities);
        void Remove(Phonebook entity);
        void RemoveRange(IEnumerable<Phonebook> entities);
    }
}
