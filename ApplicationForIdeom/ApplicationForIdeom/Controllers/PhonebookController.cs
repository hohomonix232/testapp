﻿using ApplicationForIdeom.Business.DTO;
using ApplicationForIdeom.Business.Infrastructure;
using ApplicationForIdeom.Business.Interfaces;
using ApplicationForIdeom.Domain;
using ApplicationForIdeom.Domain.Models.FilterModels;
using ApplicationForIdeom.Domain.Models.FilterModels.Base;
using ApplicationForIdeom.Domain.Models.ResponseModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationForIdeom.Controllers
{
    [Route("api/phonebooks")]
    public class PhonebookController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IPhonebookService _phonebookService;

        public PhonebookController(IPhonebookService phonebookService, IMapper mapper)
        {
            _phonebookService = phonebookService;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult Create([FromBody]PhonebookDTO phonebookDTO)
        {
            if (ModelState.IsValid)
            {
                var phonebook = _mapper.Map<Phonebook>(phonebookDTO);
                _phonebookService.Create(phonebook);
                return Ok(phonebook);
            }
            return BadRequest();
        }

        [HttpGet("{id}", Name = "GetById")]
        public async Task<Phonebook> GetById(int Id)
        {
            return await _phonebookService.GetById(Id);
        }

        [HttpGet(Name = "GetAll")]
        public async Task<IActionResult> GetAll([FromQuery] PhonebookFilterModel? filter)
        {
            var phonebooks = await _phonebookService.GetAll();
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
            var handledPhonebooks =_phonebookService.AddSelectOptions(filter, phonebooks, validFilter);
            var responseDto = _mapper.Map<IEnumerable<PhonebookDTO>>(handledPhonebooks);
            return Ok(new PagedResponse<IEnumerable<PhonebookDTO>>(responseDto, validFilter.PageNumber, validFilter.PageSize));
        }

        [HttpDelete(Name = "Delete")]
        public void Delete(int Id)
        {
            _phonebookService.Delete(Id);
        }

    }
}
