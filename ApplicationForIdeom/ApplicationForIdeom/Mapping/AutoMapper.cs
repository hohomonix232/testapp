﻿using ApplicationForIdeom.Business.DTO;
using ApplicationForIdeom.Domain;
using AutoMapper;

namespace ApplicationForIdeom.Mapping
{
    public class AutoMapper : Profile
    {
        public AutoMapper()
        {
            CreateMap<PhonebookDTO, Phonebook>().ReverseMap();
        }
    }
}
