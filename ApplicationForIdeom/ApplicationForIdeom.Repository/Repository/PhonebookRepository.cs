﻿using ApplicationForIdeom.Business.Infrastructure;
using ApplicationForIdeom.Domain;
using ApplicationForIdeom.Domain.Models.FilterModels;
using ApplicationForIdeom.Domain.Models.FilterModels.Base;
using ApplicationForIdeom.Repository.Context;
using ApplicationForIdeom.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationForIdeom.Repository.Repository
{
    public class PhonebookRepository : IPhonebookRepository
    {
        private readonly ApplicationContext _context;
        public PhonebookRepository(ApplicationContext context) : base()
        {
            _context = context;
        }

        public async void Create(Phonebook phonebook)
        {
            try 
            {
                await _context.Phonebooks.AddAsync(phonebook);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Error when trying to get all Phonebooks!", ex);
            }
        }

        public async Task<List<Phonebook>> GetAll()
        {
            try
            {
                return await _context.Phonebooks.Where(x => x.IsDeleted == false).ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Error when trying to get all Phonebooks!", ex);
            }
        }

        public async Task<Phonebook> GetById(int Id)
        {
            try
            {
                return await _context.Phonebooks.Where(x => x.IsDeleted == false && x.Id == Id).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Error when trying to get Phonebook with id: {Id}", ex);
            }
        }

        public void Delete(int Id)
        {
            try
            {
                var phonebook =_context.Phonebooks.Where(p => p.Id == Id).FirstOrDefault();
                _context.Remove(phonebook);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception($"Error when trying to delete Phonebook with id: {Id}", ex);
            }
        }

        public void Update(Phonebook phonebook)
        {
            try
            {
                _context.Phonebooks.Update(phonebook);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception($"Error when trying to update Phonebook with id {phonebook.Id}", ex);
            }
        }

        public IEnumerable<Phonebook> GetPagedData(IEnumerable<Phonebook> phonebooks, int pageNumber, int pageSize)
        {
            return pageSize != 0 ?
             phonebooks.Skip((pageNumber - 1) * pageSize).Take(pageSize) : phonebooks;
        }

        public List<Phonebook> Sorting(List<Phonebook> phonebooks, string sort)
        {
            if (!String.IsNullOrEmpty(sort))
            {
                switch (sort.ToLower())
                {
                    default:
                        phonebooks.Sort(new PhonebookSortedByFirstName());
                        break;
                }
            }
            return phonebooks;
        }

        public List<Phonebook> Filtering(List<Phonebook> phonebooks, PhonebookFilterModel filter)
        {
            if (filter != null)
            {
                if (!string.IsNullOrEmpty(filter.FirstName))
                {
                    phonebooks = phonebooks.Where(p => p.FirstName == filter.FirstName).ToList();
                }
                if (!string.IsNullOrEmpty(filter.City))
                {
                    phonebooks = phonebooks.Where(p => p.City == filter.City).ToList().ToList();
                }
                if (!string.IsNullOrEmpty(filter.ZipCode))
                {
                    phonebooks = phonebooks.Where(p => p.ZipCode == filter.ZipCode).ToList();
                }
                if (!string.IsNullOrEmpty(filter.PhoneNumber))
                {
                    phonebooks = phonebooks.Where(p => p.PhoneNumber == filter.PhoneNumber).ToList();
                }
            }
            return phonebooks;
        }
    }
}
