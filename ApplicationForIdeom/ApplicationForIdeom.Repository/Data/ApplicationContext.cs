﻿using ApplicationForIdeom.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationForIdeom.Repository.Context
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Phonebook> Phonebooks { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }
    }
}
