﻿using ApplicationForIdeom.Domain;
using ApplicationForIdeom.Domain.Models.FilterModels;
using System.Collections.Generic;

namespace ApplicationForIdeom.Repository.Interfaces
{
    public interface IPhonebookRepository : IRepository<Phonebook>
    {
        public IEnumerable<Phonebook> GetPagedData(IEnumerable<Phonebook> phonebooks, int pageNumber, int pageSize);

        public List<Phonebook> Sorting(List<Phonebook> phonebooks, string sort);

        public List<Phonebook> Filtering(List<Phonebook> phonebooks, PhonebookFilterModel filter);
    }
}
