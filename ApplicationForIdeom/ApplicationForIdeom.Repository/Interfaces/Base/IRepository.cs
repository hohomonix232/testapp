﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationForIdeom.Repository.Interfaces
{
    public interface IRepository<T>
    {
        public void Create(T entity);

        public void Update(T entity);

        public Task<List<T>> GetAll();

        public Task<T> GetById(int Id);

        public void Delete(int Id);
    }
}
