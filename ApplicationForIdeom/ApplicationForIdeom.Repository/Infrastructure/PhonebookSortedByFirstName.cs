﻿using ApplicationForIdeom.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationForIdeom.Business.Infrastructure
{
    public class PhonebookSortedByFirstName : IComparer<Phonebook>
    {
        public int Compare(Phonebook x, Phonebook y)
        {
            return x.FirstName.CompareTo(y.FirstName);
        }
    }
}
