﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationForIdeom.Domain.Models.FilterModels
{
    public class PhonebookFilterModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Sort { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
    }
}
