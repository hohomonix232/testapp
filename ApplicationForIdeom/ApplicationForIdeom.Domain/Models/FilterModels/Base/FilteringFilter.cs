﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationForIdeom.Domain.Models.FilterModels.Base
{
    public class FilteringFilter
    {
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
    }
}
