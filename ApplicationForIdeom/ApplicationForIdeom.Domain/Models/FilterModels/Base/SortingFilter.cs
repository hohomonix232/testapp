﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationForIdeom.Domain.Models.FilterModels.Base
{
    public class SortingFilter
    {
        public string Sort { get; set; }
    }
}
