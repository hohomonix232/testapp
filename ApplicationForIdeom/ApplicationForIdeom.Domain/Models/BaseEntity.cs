﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationForIdeom.Domain
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
